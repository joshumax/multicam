namespace ASCOM.MultiCam
{
    partial class SetupDialogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdOK = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.titleLabel = new System.Windows.Forms.Label();
            this.picASCOM = new System.Windows.Forms.PictureBox();
            this.secondaryCamera = new System.Windows.Forms.ComboBox();
            this.maxTimeFrameMerge = new System.Windows.Forms.NumericUpDown();
            this.coolerOffset = new System.Windows.Forms.NumericUpDown();
            this.coolerOffsetLabel = new System.Windows.Forms.Label();
            this.chkTrace = new System.Windows.Forms.CheckBox();
            this.primaryCamera = new System.Windows.Forms.ComboBox();
            this.requireTemp = new System.Windows.Forms.CheckBox();
            this.useAbsolute = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.secondaryCameraLabel = new System.Windows.Forms.Label();
            this.primaryCameraLabel = new System.Windows.Forms.Label();
            this.maxTimeLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSaveDimens = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picASCOM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxTimeFrameMerge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coolerOffset)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdOK
            // 
            this.cmdOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOK.Location = new System.Drawing.Point(281, 290);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(59, 24);
            this.cmdOK.TabIndex = 0;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = true;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(281, 320);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(59, 25);
            this.cmdCancel.TabIndex = 1;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // titleLabel
            // 
            this.titleLabel.Location = new System.Drawing.Point(12, 9);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(274, 22);
            this.titleLabel.TabIndex = 2;
            this.titleLabel.Text = "MultiCam - The multi-camera ASCOM hub";
            // 
            // picASCOM
            // 
            this.picASCOM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picASCOM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picASCOM.Image = global::ASCOM.MultiCam.Properties.Resources.ASCOM;
            this.picASCOM.Location = new System.Drawing.Point(227, 289);
            this.picASCOM.Name = "picASCOM";
            this.picASCOM.Size = new System.Drawing.Size(48, 56);
            this.picASCOM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picASCOM.TabIndex = 3;
            this.picASCOM.TabStop = false;
            this.picASCOM.Click += new System.EventHandler(this.BrowseToAscom);
            this.picASCOM.DoubleClick += new System.EventHandler(this.BrowseToAscom);
            // 
            // secondaryCamera
            // 
            this.secondaryCamera.FormattingEnabled = true;
            this.secondaryCamera.Location = new System.Drawing.Point(129, 32);
            this.secondaryCamera.Name = "secondaryCamera";
            this.secondaryCamera.Size = new System.Drawing.Size(90, 21);
            this.secondaryCamera.TabIndex = 9;
            this.secondaryCamera.Click += new System.EventHandler(this.secondaryCamera_Click);
            // 
            // maxTimeFrameMerge
            // 
            this.maxTimeFrameMerge.Location = new System.Drawing.Point(6, 45);
            this.maxTimeFrameMerge.Name = "maxTimeFrameMerge";
            this.maxTimeFrameMerge.Size = new System.Drawing.Size(95, 20);
            this.maxTimeFrameMerge.TabIndex = 10;
            this.maxTimeFrameMerge.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // coolerOffset
            // 
            this.coolerOffset.Location = new System.Drawing.Point(6, 45);
            this.coolerOffset.Name = "coolerOffset";
            this.coolerOffset.Size = new System.Drawing.Size(95, 20);
            this.coolerOffset.TabIndex = 12;
            // 
            // coolerOffsetLabel
            // 
            this.coolerOffsetLabel.AutoSize = true;
            this.coolerOffsetLabel.Location = new System.Drawing.Point(3, 16);
            this.coolerOffsetLabel.Name = "coolerOffsetLabel";
            this.coolerOffsetLabel.Size = new System.Drawing.Size(96, 26);
            this.coolerOffsetLabel.TabIndex = 13;
            this.coolerOffsetLabel.Text = "Secondary camera\r\ncooler offset";
            // 
            // chkTrace
            // 
            this.chkTrace.AutoSize = true;
            this.chkTrace.Location = new System.Drawing.Point(243, 266);
            this.chkTrace.Name = "chkTrace";
            this.chkTrace.Size = new System.Drawing.Size(60, 17);
            this.chkTrace.TabIndex = 19;
            this.chkTrace.Text = "Trace?";
            this.chkTrace.UseVisualStyleBackColor = true;
            // 
            // primaryCamera
            // 
            this.primaryCamera.FormattingEnabled = true;
            this.primaryCamera.Location = new System.Drawing.Point(9, 32);
            this.primaryCamera.Name = "primaryCamera";
            this.primaryCamera.Size = new System.Drawing.Size(90, 21);
            this.primaryCamera.TabIndex = 7;
            this.primaryCamera.Click += new System.EventHandler(this.primaryCamera_Click);
            // 
            // requireTemp
            // 
            this.requireTemp.AutoSize = true;
            this.requireTemp.Location = new System.Drawing.Point(4, 107);
            this.requireTemp.Name = "requireTemp";
            this.requireTemp.Size = new System.Drawing.Size(92, 17);
            this.requireTemp.TabIndex = 15;
            this.requireTemp.Text = "Require temp.";
            this.requireTemp.UseVisualStyleBackColor = true;
            // 
            // useAbsolute
            // 
            this.useAbsolute.AutoSize = true;
            this.useAbsolute.Location = new System.Drawing.Point(4, 71);
            this.useAbsolute.Name = "useAbsolute";
            this.useAbsolute.Size = new System.Drawing.Size(89, 30);
            this.useAbsolute.TabIndex = 14;
            this.useAbsolute.Text = "Use absolute\r\nvalue instead";
            this.useAbsolute.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.secondaryCameraLabel);
            this.groupBox1.Controls.Add(this.primaryCameraLabel);
            this.groupBox1.Controls.Add(this.primaryCamera);
            this.groupBox1.Controls.Add(this.secondaryCamera);
            this.groupBox1.Location = new System.Drawing.Point(4, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(229, 65);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Camera Selection";
            // 
            // secondaryCameraLabel
            // 
            this.secondaryCameraLabel.AutoSize = true;
            this.secondaryCameraLabel.Location = new System.Drawing.Point(126, 16);
            this.secondaryCameraLabel.Name = "secondaryCameraLabel";
            this.secondaryCameraLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.secondaryCameraLabel.Size = new System.Drawing.Size(97, 13);
            this.secondaryCameraLabel.TabIndex = 8;
            this.secondaryCameraLabel.Text = "Secondary Camera";
            // 
            // primaryCameraLabel
            // 
            this.primaryCameraLabel.AutoSize = true;
            this.primaryCameraLabel.Location = new System.Drawing.Point(6, 16);
            this.primaryCameraLabel.Name = "primaryCameraLabel";
            this.primaryCameraLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.primaryCameraLabel.Size = new System.Drawing.Size(80, 13);
            this.primaryCameraLabel.TabIndex = 5;
            this.primaryCameraLabel.Text = "Primary Camera";
            // 
            // maxTimeLabel
            // 
            this.maxTimeLabel.AutoSize = true;
            this.maxTimeLabel.Location = new System.Drawing.Point(3, 16);
            this.maxTimeLabel.Name = "maxTimeLabel";
            this.maxTimeLabel.Size = new System.Drawing.Size(98, 26);
            this.maxTimeLabel.TabIndex = 11;
            this.maxTimeLabel.Text = "Maximum time\r\nbefore frame merge";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.maxTimeLabel);
            this.groupBox2.Controls.Add(this.maxTimeFrameMerge);
            this.groupBox2.Location = new System.Drawing.Point(239, 34);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(105, 80);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Plate Solving";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.coolerOffsetLabel);
            this.groupBox3.Controls.Add(this.requireTemp);
            this.groupBox3.Controls.Add(this.coolerOffset);
            this.groupBox3.Controls.Add(this.useAbsolute);
            this.groupBox3.Location = new System.Drawing.Point(239, 120);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(105, 140);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cooling";
            // 
            // btnSaveDimens
            // 
            this.btnSaveDimens.ForeColor = System.Drawing.Color.Red;
            this.btnSaveDimens.Location = new System.Drawing.Point(133, 289);
            this.btnSaveDimens.Name = "btnSaveDimens";
            this.btnSaveDimens.Size = new System.Drawing.Size(88, 56);
            this.btnSaveDimens.TabIndex = 22;
            this.btnSaveDimens.Text = "SAVE DIMENSIONS";
            this.btnSaveDimens.UseVisualStyleBackColor = true;
            // 
            // SetupDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 353);
            this.Controls.Add(this.btnSaveDimens);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkTrace);
            this.Controls.Add(this.picASCOM);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SetupDialogForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MultiCam Setup";
            ((System.ComponentModel.ISupportInitialize)(this.picASCOM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxTimeFrameMerge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coolerOffset)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.PictureBox picASCOM;
        private System.Windows.Forms.ComboBox secondaryCamera;
        private System.Windows.Forms.NumericUpDown maxTimeFrameMerge;
        private System.Windows.Forms.NumericUpDown coolerOffset;
        private System.Windows.Forms.Label coolerOffsetLabel;
        private System.Windows.Forms.CheckBox chkTrace;
        private System.Windows.Forms.ComboBox primaryCamera;
        private System.Windows.Forms.CheckBox requireTemp;
        private System.Windows.Forms.CheckBox useAbsolute;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label secondaryCameraLabel;
        private System.Windows.Forms.Label primaryCameraLabel;
        private System.Windows.Forms.Label maxTimeLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSaveDimens;
    }
}
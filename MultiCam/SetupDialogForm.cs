using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using ASCOM.Utilities;
using ASCOM.MultiCam;

namespace ASCOM.MultiCam
{
    [ComVisible(false)]					// Form not registered for COM!
    public partial class SetupDialogForm : Form
    {
        public SetupDialogForm()
        {
            InitializeComponent();
            // Initialise current values of user settings from the ASCOM Profile
            InitUI();
        }

        private void cmdOK_Click(object sender, EventArgs e) // OK button event handler
        {
            // Place any validation constraint checks here
            // Update the state variables with results from the dialogue
            Camera.comPort = (string)primaryCamera.SelectedItem;
            Camera.tl.Enabled = chkTrace.Checked;

            string primaryCamID = (string) primaryCamera.Items[0];
            string secondaryCamID = (string) secondaryCamera.Items[0];
            if (!String.IsNullOrEmpty(primaryCamID) && !String.IsNullOrEmpty(secondaryCamID))
            {
                Camera.primaryCameraID = primaryCamID;
                Camera.secondaryCameraID = secondaryCamID;
            }
        }

        private void cmdCancel_Click(object sender, EventArgs e) // Cancel button event handler
        {
            Close();
        }

        private void BrowseToAscom(object sender, EventArgs e) // Click on ASCOM logo event handler
        {
            try
            {
                System.Diagnostics.Process.Start("http://ascom-standards.org/");
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                    MessageBox.Show(noBrowser.Message);
            }
            catch (System.Exception other)
            {
                MessageBox.Show(other.Message);
            }
        }

        private void InitUI()
        {
            chkTrace.Checked = Camera.tl.Enabled;
            // set the list of com ports to those that are currently available
            /*primaryCamera.Items.Clear();
            primaryCamera.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames());      // use System.IO because it's static
            // select the current port if possible
            if (primaryCamera.Items.Contains(Camera.comPort))
            {
                primaryCamera.SelectedItem = Camera.comPort;
            }*/
        }

        private void primaryCamera_Click(object sender, EventArgs e)
        {
            primaryCamera.Items.Clear();
            primaryCamera.Items.Add(ASCOM.DriverAccess.Camera.Choose(""));
            primaryCamera.SelectedIndex = 0;
        }

        private void secondaryCamera_Click(object sender, EventArgs e)
        {
            secondaryCamera.Items.Clear();
            secondaryCamera.Items.Add(ASCOM.DriverAccess.Camera.Choose(""));
            secondaryCamera.SelectedIndex = 0;
        }
    }
}